<!doctype html>
<html>
<head>
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width,inital-scale=1.0">
 <link href="public/app.css" rel="stylesheet" />
 <title>Home - Eventbrite</title>
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
</head>
<body>
 <div class="container">
   <div class="row">
     <?php include 'src/views/pages/'.$current_view; ?>
   </div>
 </div>
</body>
</html>
