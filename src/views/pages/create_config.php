<?php
/**
 *  This page will dynamically ask for the required config that are missing
 */

$db_config = [
  'title_db' => 'Database Configuration',
  'DB_HOST' => 'localhost',
  'DB_USER' => 'root',
  'DB_PASS' => 'password',
  'DB_NAME' => 'eventbrite'
];

$other_config = [
  'title_rev' => 'Eventbrite\'s details',
  'EVENTBRITE_TOKEN' => 'Hash token',
  'EVENTBRITE_API_URL' => 'https://eventbrite.com/api/v3/',
  'title_autopilot' => 'Autopilot\'s details',
  'AUTOPILOT_TOKEN' => 'Hash token',
  'AUTOPILOT_API_URL' => 'https://private-anon-38b9e523fb-autopilot.apiary-mock.com/v1/'
];


$needed_config = file_exists('src/db_config.php') ? $other_config : $db_config;

echo Utils::generateForm($_ENV, $needed_config);
