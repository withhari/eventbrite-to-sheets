<?php

class AutopilotApi extends BaseApiRequest
{
  private static $instance = null;

  public static function singleton()
  {
    if (self::$instance == null) {
      $default_headers = array(
        'autopilotapikey' => $_ENV['AUTOPILOT_TOKEN'],
        'Content-Type' => 'application/json'
      );
      self::$instance = new self($_ENV['AUTOPILOT_API_URL'], $default_headers);
    }
    return self::$instance;
  }
  
  /**
   * Sends bulk add request to the autopilot API for given list id.
   * 
   * @see http://docs.autopilot.apiary.io/#reference/api-methods/bulk-add-contacts/bulk-add-contacts
   * 
   * @param array $contacts
   * @return void
   */
  public function addContacts($contacts = array())
  {
    $json = json_encode(['contacts' => $contacts]);
    $response = $this->post('/contacts', $json);
    var_dump($response->body);
  }

}