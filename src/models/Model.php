<?php

class Model
{
  private $data = [];

  public function __construct()
  {

  }

  public function __set($name, $value)
  {
    $this->data[$name] = $value;
  }

  public function __get($name) {
    if (array_key_exists($name, $this->data)) {
      return htmlentities($this->data[$name], ENT_QUOTES);
    }
    return null;
  }

}