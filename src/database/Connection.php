<?php

class Connection
{

    private static $connection;

    private function __construct()
    {
    }

    public static function fetch($sql)
    {
        $stm = self::get()->prepare($sql);
        if ($stm->execute()) {
            return $stm->fetchAll();
        }
        return [];
    }
    
    /**
     * Executes given SQL statement and returns result
     *
     * @param string $sql The SQL code to execute
     * @return boolean true if successfully executed
     */
    public static function execute($sql) {
        return self::get()->prepare($sql)->execute();
    }

    public static function get()
    {
        if (self::$connection == null) {
            $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
            $count = 0;
            do {
                try {
                    $count++;
                    self::$connection = new \PDO($dsn, DB_USER, DB_PASS);
                    self::$connection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
                } catch (Exception $e) {
                    self::$connection = null;
                }
            } while (self::$connection == null || $count < 5);
        }
        return self::$connection;
    }

    public static function close()
    {
        if (self::$connection != null) {
            $connection->close();
        }
    }
}
