<?php

$_ENV = []; //all the configurations will be read to this array from database
$current_view = 'create_config.php';

require_once 'vendor/autoload.php';
require_once 'autoload.php';
if (file_exists('src/db_config.php')) {
  require_once 'src/db_config.php';
  $_ENV = Utils::loadConfig();
  if (count($_ENV) >= 6) {
    $current_view = 'show.php';
  }
}

if (isset($_POST) && count($_POST) > 0) {
  $current_view = 'process_request.php';
}

include 'src/views/index.php';